// 1 to 5
var defconLevelColors = ["green","yellow","orange","darkorange","red"];
var domainMessages = [
    "Doména obsahuje pravdivé informácie",
    "Doména môže obsahovať propagandu",
    "Doména obsahuje propagandu",
    "Doména obsahuje masívnu propagandu",
    "Doména je prevádzkovaná za účelom propagandy"
];
var pageMessages = [
    "Stránka obsahuje pravdivé informácie",
    "Stránka môže obsahovať propagandu",
    "Stránka obsahuje propagandu",
    "Stránka obsahuje masívnu propagandu",
    "Stránka je prevádzkovaná za účelom propagandy"
];

function showNotificationBar(description, type, defconlevel) {
    var bgColor = defconLevelColors[defconlevel - 1];
    /*set default values*/
    var forbidMessage;

    if(type == "page")
    {
        forbidMessage = "<span id='forbid-showing-page'>"+ pageMessages[defconlevel - 1] +"</span>";
    }
    else
    {
        forbidMessage = "<span id='forbid-showing-domain'>"+ domainMessages[defconlevel - 1] +"</span>";
    }

    if ($('#antipro-notification-bar').size() == 0) {
        var HTMLmessage = "<div id='antipro-notification'><div class='antipro-top'>Pozor!</div><div class='antipro-message'> " + forbidMessage + "</div>";
        HTMLmessage += '<div class="antipro-description">'+description+'</div>';
        $('body').append("<div id='antipro'><div id='antipro-notification-bar' class='antipro-color-" + bgColor + "'>" + HTMLmessage + "</div></div>");
    }
    /*animate the bar*/
    $('#notification-bar').slideDown();

    // spracovanie kliku na odkaz
    if(type == "page")
    {
        document.getElementById("forbid-showing-page").addEventListener("click", forbidShowingPage, false);
    }
    else
    {
        document.getElementById("forbid-showing-domain").addEventListener("click", forbidShowingDomain, false);
    }
}

// spracovanie spravy od extension
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    chrome.storage.local.get('list', function (result) {
        
        var showWarning = false;
        var generalIndex = -1;
        var concreteIndex = -1;

        $.each(result.list, function(index, value) {
            // ak sa nema tento warning zobrazovat tak zbytocne kontrolujeme dalej
            var testUrl = parseUri(result.list[index].url);

            if(testUrl.path == "")
            {

                if($(location).attr('hostname') == testUrl.host)
                {
                    showWarning = true;
                    generalIndex = index;
                }
            }
            else
            {
                // +2 pre // za protocolom
                var substrLength = $(location).attr('protocol').length + 2;
                var locationSource = $(location).attr('href').substring(substrLength);

                if(testUrl.source == locationSource)
                {
                    showWarning = true;
                    concreteIndex = index;
                }
            }
        });

        if(showWarning == true)
        {
            if(concreteIndex != -1)
            {
                if(result.list[concreteIndex].display == "1")
                {
                    showNotificationBar(result.list[concreteIndex].description, "page", result.list[concreteIndex].defconlevel);
                }
                else
                {
                    if(generalIndex != -1)
                    {
                        if(result.list[generalIndex].display == "1")
                        {
                            showNotificationBar(result.list[generalIndex].description, "domain" , result.list[generalIndex].defconlevel);
                        }
                    }
                }

                chrome.runtime.sendMessage({defconLevel: result.list[concreteIndex].defconlevel, show: true});
            }
            else
            {
                if(generalIndex != -1)
                {
                    if(result.list[generalIndex].display == "1")
                    {
                        showNotificationBar(result.list[generalIndex].description, "domain", result.list[generalIndex].defconlevel);
                    }

                    chrome.runtime.sendMessage({defconLevel: result.list[generalIndex].defconlevel, show: true});   
                }
            }
        }
    });
});

function forbidShowingPage()
{
    $("#notification-bar").slideUp();

    // +2 pre // za protocolom
    var substrLength = $(location).attr('protocol').length + 2;
    var locationSource = $(location).attr('href').substring(substrLength);

    chrome.storage.local.get('list', function (result) {
        for(i = 0; i < result.list.length; i++)
        {
            if(result.list[i].url == locationSource)
            {
                result.list[i].display = "0";
                break;
            }
        }

        chrome.storage.local.set({'list': result.list});
    });
}

function forbidShowingDomain()
{
    $("#notification-bar").slideUp();

    chrome.storage.local.get('list', function (result) {
        for(i = 0; i < result.list.length; i++)
        {
            console.log(result.list[i].url + " " + $(location).attr('hostname'));
            if(result.list[i].url == $(location).attr('hostname'))
            {
                result.list[i].display = "0";
                break;
            }
        }

        chrome.storage.local.set({'list': result.list});
    });
}


