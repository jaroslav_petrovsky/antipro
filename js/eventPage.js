var URL = "http://antipro.ixy.sk/getlist.php";

function onCompleted(details) {
    // kontrola aktualnosti zoznamu stranok
    checkDate();

    // nacitanie stranok
    // chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    // chrome.tabs.query({}, function(tabs) {
        // for(i = 0; i < tabs.length; i++)
        // {
                chrome.tabs.sendMessage(details.tabId, {});
            // }
        // }
    // });
}

function getList() {
    // $.getJSON('https://spreadsheets.google.com/feeds/list/19F4O36xeKv-A39bdvMB9EhMl2vY1C_LVGkFnLKJiq5M/1/public/values?alt=json', updateList);
    $.getJSON(URL, updateList);
}

// callback
function updateList(data) {
    var list = new Array();

    for (index = 0; index < data.length; index++) {
        list[list.length] = data[index];
    }

    // chrome.storage.sync.set({"list": list});

    // synchornizacia online dat z local storage
    chrome.storage.local.get('list', function (result) {
        // ak sa vidime prvy raz
        if($.isEmptyObject(result))
        {
            chrome.storage.local.set({'list': list});
            console.log(result.list);
        }
        else
        {
            syncLists(list, result.list);
        }
    });
    
}

// funkcia pre zobrazenie ikonky
function messageFromContentScript(request, sender, sendResponse)
{
    if(request.show == true)
    {
        var icons = ["green","yellow","orange","darkorange","red"];
        var pathToIcon = "img/icon-" + icons[request.defconLevel - 1] + ".png";

        chrome.pageAction.setIcon({tabId: sender.tab.id, path: pathToIcon});
        chrome.pageAction.show(sender.tab.id);

    }
}

chrome.webNavigation.onCompleted.addListener(onCompleted);

// listener zobrazenie ikonky
chrome.runtime.onMessage.addListener(messageFromContentScript);

function checkDate()
{
    var currentTime = new Date();
    var lastUpdateTime = new Date();

    chrome.storage.local.get('lastUpdate', function (result) {

        if(!$.isEmptyObject(result))
        {
            lastUpdateTime = new Date(result.lastUpdate);
            
            var currentDateString = currentTime.getDate() + "" + currentTime.getMonth() + "" + currentTime.getFullYear();
            var updateDateString = lastUpdateTime.getDate() + "" + lastUpdateTime.getMonth() + "" + lastUpdateTime.getFullYear();

            if( currentDateString != updateDateString )
            {
                chrome.storage.local.set({'lastUpdate': currentTime.getTime()});
                getList();
            }

        }
        else
        {
            chrome.storage.local.set({'lastUpdate': currentTime.getTime()});
            getList();
        }
    });
}

function syncLists(newData, currenData)
{
    var list = new Array();

    // zotriedim polia
    newData.sort(compare);
    currenData.sort(compare);

    // pozicia v starych datach
    var j = 0;

    for (i = 0; i < newData.length; i++) {
        // vlozim objekt
        list[list.length] = 
        {
            timestamp:      newData[i].timestamp,
            url:            newData[i].url,
            description:    newData[i].description, 
            responsible:    newData[i].responsible,
            defconlevel:    newData[i].defconlevel,
            display:       "1"
        };

        // ak objekt uz existval, tak nastavim display flag
        // a skocime aj na dalsi ovjekt v starom poli
        if(j < currenData.length)
        {
            if(compare(currenData[j], newData[j]) == 0)
            {
                // -1 pretoze chcem prvok ktory sme vlozili pred chvilkou, nie dalsi
                list[list.length - 1].display = currenData[i].display;
                j++;
            }
        }
    }

    // chrome.storage.sync.get('list', function (result) {
    // // ak sa vidime prvy raz
    // if($.isEmptyObject(result))
    // {
    //     var displayList = new Array();

    //     for(i = 0; i < list.length; i++)
    //     {
    //         displayList[i] =
    //         {
    //             url:        url[i].url,
    //             display:    list[i].display,
    //         }
    //     }

    //     chrome.storage.sync.set({'list': list});
    // }
    // else
    // {

    //     syncLists(list, result.list);
    // }
    // });

    chrome.storage.local.set({'list': list});
}

function compare(a,b) 
{
    if (a.url > b.url)
    {
        return -1;
    }

    if (a.url < b.url)
    {
        return 1;
    }

    return 0;
}